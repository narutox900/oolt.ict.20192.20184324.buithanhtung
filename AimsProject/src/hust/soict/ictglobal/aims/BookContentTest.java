package hust.soict.ictglobal.aims;

import hust.soict.ictglobal.aims.media.Book;

import java.util.Arrays;

public class BookContentTest {
    public static void main(String[] args) {
        Book b = new Book("a", "b", Arrays.asList("c"), 50f);
        b.setContent("abc, efg! hik ghk");
        b.setContent("tung, tung! tung!!?? tung tung tung abcd");
    }
}

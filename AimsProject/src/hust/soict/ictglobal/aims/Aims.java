package hust.soict.ictglobal.aims;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.PlayerException;
import hust.soict.ictglobal.aims.order.*;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Aims {
    public static void showMenu() {

        System.out.println("Order Management Application: ");

        System.out.println("--------------------------------");

        System.out.println("1. Create new order");

        System.out.println("2. Add item to the order");

        System.out.println("3. Delete item by id");

        System.out.println("4. Display the items list of order");

        System.out.println("0. Exit");

        System.out.println("--------------------------------");

        System.out.println("Please choose a number: 0-1-2-3-4 ");

    }

    public static void main(String[] args) throws PlayerException {
        int n = 0;
        Thread daemonThread = new Thread(new MemoryDaemon());
        daemonThread.setDaemon(true);
        daemonThread.start();
        // Sometimes the program prints the memory used

        Order order = null;
        LocalDate date = LocalDate.now();
        Scanner keyboard = new Scanner(System.in);
        do {
            showMenu();
            n = keyboard.nextInt();
            switch (n) {
                case 1:
                    order = Order.createOrder(date);
                    System.out.println("New order has been created.");
                    break;
                case 2:
                    addItem(order);
                    break;

                case 3:
                    System.out.println("Enter the index of the item you want to delete: ");
                    int i = keyboard.nextInt();
                    order.removeMedia(i);
                    break;
                case 4:
                    order.printOrder();
                    break;
                case 0:
                    System.out.println("Thank you, please come again.");
                default:
                    System.out.println("Invalid input, please try again");
                    break;

            }

        } while (n != 0);

        //I included the test cases for compareTo() method in the CompareTestcase.java

        return;

    }

    private static Order addItem(Order order) throws PlayerException {
        if (order == null) {
            System.out.println("Order has not been created, please make one.");
            return order;
        }
        Scanner keyboard = new Scanner(System.in);
        System.out.print("How many items do you want to add? ");
        int j = keyboard.nextInt();
        for (int i = 0; i < j; i++) {
            System.out.print("Do you want to order a book or a DVD (Type 1 for book, 2 for DVD, 3 for Compact Disc): ");
            int o = keyboard.nextInt();
            if (o == 1) {
                keyboard.nextLine();
                System.out.print("Enter the title: ");
                String name = keyboard.nextLine();
                System.out.print("Enter the category: ");
                String cat = keyboard.nextLine();
                System.out.print("Enter the author name: ");
                String auth = keyboard.nextLine();
                System.out.print("Enter the cost: ");
                float cost = keyboard.nextFloat();
                order.addMedia(new Book(name, cat, Arrays.asList(auth), cost));
            } else if (o == 2) {
                keyboard.nextLine();
                System.out.print("Enter the title: ");
                String name = keyboard.nextLine();
                System.out.print("Enter the category: ");
                String cat = keyboard.nextLine();
                System.out.print("Enter the director's name: ");
                String dir = keyboard.nextLine();
                System.out.print("Enter the length: ");
                int length = keyboard.nextInt();
                System.out.print("Enter the cost: ");
                float cost = keyboard.nextFloat();
                order.addMedia(new DigitalVideoDisc(name, cat, dir, length, cost));
            } else if (o == 3) {

                keyboard.nextLine();
                System.out.print("Enter the title: ");
                String name = keyboard.nextLine();
                System.out.print("Enter the category: ");
                String cat = keyboard.nextLine();
                System.out.print("Enter the artist's name: ");
                String art = keyboard.nextLine();
                System.out.print("Enter the director's name: ");
                String dir = keyboard.nextLine();
                System.out.print("Enter the cost: ");
                float cost = keyboard.nextFloat();
                order.addMedia(new CompactDisc(name, cat, art, dir, cost));
            }
            if (o == 2 || o == 3) {
                System.out.print("Do you want to play the disk? (1 for yes 2 for no) ");
                o = keyboard.nextInt();
                if (o == 1) {
                    try {
                        order.play();
                    } catch (PlayerException e) {
                        e.getMessage();
                        e.toString();
                    }
                }
            }
        }
        return order;

    }
}

package hust.soict.ictglobal.aims;

public class MemoryDaemon extends Thread {
    public MemoryDaemon() {
        super();
    }

    long memoryUsed = 0;

    public void run() {
        Runtime rt = Runtime.getRuntime();
        long used;
        while (true) {
            used = rt.totalMemory() - rt.freeMemory();
            if (used != memoryUsed) {
                System.out.println("Memory used = " + used);
                memoryUsed = used;
            }
        }
    }


}

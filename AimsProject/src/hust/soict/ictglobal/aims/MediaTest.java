package hust.soict.ictglobal.aims;

import java.sql.Array;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.order.*;

public class MediaTest {
    public static void main(String[] args) {
        LocalDate date = LocalDate.now();

        Order order1 = Order.createOrder(date);


        order1.addMedia(new DigitalVideoDisc("My Fair Lady", "BumBumBUm", "Me", 140, 20.0f));
        order1.addMedia(new DigitalVideoDisc("Eternal Flame", "Horror", "Not me", 220, 30f));

        DigitalVideoDisc d1 = new DigitalVideoDisc("Crying", "Tragedy", "Envy", 200, 50f);
        DigitalVideoDisc d2 = new DigitalVideoDisc("WOW", "LONG", "NO", 111, 17f);
        order1.addMedia(d1, d2);
        DigitalVideoDisc d3 = new DigitalVideoDisc("test 3", "testing", "btt", 120, 12f);
        DigitalVideoDisc d4 = new DigitalVideoDisc("test 4", "testing", "btt", 120, 12f);
        DigitalVideoDisc d5 = new DigitalVideoDisc("test 5", "testing", "btt", 120, 12f);

        order1.addMedia(d3, d4, d5 );
        Book b1 = new Book("Rain","Romance", Arrays.asList("btt","btt2","btt3"),15f);
        Book b2 = new Book("Cactus","Comedy", Arrays.asList("btt","btt2"),15f);
        b2.addAuthor("notbtt");
        b1.removeAuthor("btt");
        order1.addMedia(b1,b2);
        order1.addMedia(d5,b1,b2,  d4, d3);
        order1.printOrder();
        Media lucky = order1.getALuckyItem();
        System.out.println("Congratulations, you got " + lucky.getTitle() + " for free!");
        System.out.println("New total cost is: " + (order1.totalCost() - lucky.getCost()));

        System.out.println("\nSearch for \"test\" in the order: ");

        order1.search("test");
        System.out.println("\nSearch for \"WO\" in the order:");
        order1.search("WO");
    }

}

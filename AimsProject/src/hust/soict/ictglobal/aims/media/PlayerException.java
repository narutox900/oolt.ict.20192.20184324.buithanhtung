package hust.soict.ictglobal.aims.media;

public class PlayerException extends Exception {
    public PlayerException(String msg) {
        super(msg);
    }

    public PlayerException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public PlayerException() {
        super();
    }
}


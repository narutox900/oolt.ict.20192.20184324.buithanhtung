package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.Scanner;

public class CompactDisc extends Disc implements Playable, Comparable<Media> {

    private String artist;
    private int length;
    private ArrayList<Track> tracks = new ArrayList<Track>();

    Scanner keyboard = new Scanner(System.in);

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public CompactDisc(String title) {
        this.setTitle(title);
    }

    public int getNumberoftracks() {
        return this.tracks.size();
    }


    public CompactDisc(String title, float cost) {
        this.setTitle(title);
        this.setCost(cost);
    }

    public CompactDisc(String title, String category, String artist, String director, float cost) {
        this.setTitle(title);
        this.setCategory(category);
        this.setDirector(director);
        this.artist = artist;
        this.setCost(cost);
        System.out.print("Enter the number of track: ");
        int num = keyboard.nextInt();
        for (int i = 0; i < num; i++) {
            keyboard.nextLine();
            addTrack();
        }
        this.setLength(this.getLength());
    }

    public CompactDisc(String title, String category, String director, String artist, int length, ArrayList<Track> tracks) {
        this.setTitle(title);
        this.setCategory(category);
        this.setDirector(director);
        this.artist = artist;
        this.length = length;
        this.tracks = tracks;
    }

    public void addTrack() {

        Track track = new Track();
        System.out.print("Enter the track's name: ");
        track.setTitle(keyboard.nextLine());
        System.out.print("Enter the track's length: ");
        track.setLength(keyboard.nextInt());
        if (this.tracks.contains(track))
            System.out.println("The track already existed.");
        else
            this.tracks.add(track);
    }

    public void removeTrack() {

        Track track = new Track();
        System.out.print("Enter the track's name: ");
        track.setTitle(keyboard.nextLine());
        System.out.print("Enter the track's length: ");
        track.setLength(keyboard.nextInt());
        if (this.tracks.remove(track))
            System.out.println("Track has been removed.");
        else
            System.out.println("Track doesn't exists.");

    }

    public int getLength() {
        int length = 0;
        for (int i = 0; i < this.tracks.size(); i++) {
            length += this.tracks.get(i).getLength();
        }
        return length;
    }


    @Override
    public void play() throws PlayerException {
        if (this.getLength() <= 0) {
            throw new PlayerException("Error: CD length is 0");
        }
        System.out.println("Playing CD: " + this.getTitle());
        System.out.println("The disk has " + this.tracks.size() + "tracks and has a total length of " + this.getLength() + ".");
        for (int i = 0; i < this.tracks.size(); i++) {
            try {
                tracks.get(i).play();
            } catch (PlayerException e) {
                e.printStackTrace();
            }
        }
    }


    //Compare by number of tracks and length
    @Override
    public int compareTo(Media o) {
        CompactDisc object = (CompactDisc) o;
        if (this.getNumberoftracks() == object.getNumberoftracks())
            return ((Integer) this.getLength()).compareTo((Integer) object.getLength());
        else return ((Integer) this.getNumberoftracks()).compareTo((Integer) object.getNumberoftracks());

    }
}

package hust.soict.ictglobal.aims.media;

import java.util.*;

public class Book extends Media implements Comparable<Media> {


    private List<String> authors;
    private String content;
    private List<String> contentTokens;
    private Map<Object, Integer> wordFrequency = new HashMap<>();

    public Book(String title, String category, List<String> authors, float cost) {
        this.setTitle(title);
        this.setCategory(category);
        this.setCost(cost);
        this.authors = authors;
    }


    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public void addAuthor(String authorName) {
        boolean b;
        if (this.authors.contains(authorName))
            System.out.println("This author's name is already added");
        else {
            this.authors = new ArrayList<String>(authors);
            this.authors.add(authorName);

        }

    }

    public void removeAuthor(String authorName) {
        if (this.authors.contains(authorName)) {
            this.authors = new ArrayList<String>(authors);
            this.authors.remove(authorName);
        } else System.out.println("This author's name is not in the list");
    }

    public void setContent(String content) {
        this.content = content;
        processContent();
        System.out.println(wordFrequency);
    }

    public void processContent() {
        wordFrequency.clear();
        contentTokens = Arrays.asList(content.split("[ .,!?@\n]+"));
        Collections.sort(contentTokens);
        for (String token : contentTokens) {
            if (wordFrequency.containsKey(token)) {
                wordFrequency.put(token, wordFrequency.get(token) + 1);
            } else
                wordFrequency.put(token, 1);

        }

    }


    //Books are compared by cost
    @Override
    public int compareTo(Media o) {
        Book object = (Book) o;
        if (this.getCost() == object.getCost())
            return 0;
        else if (this.getCost() < object.getCost())
            return -1;
        else return 1;

    }

}


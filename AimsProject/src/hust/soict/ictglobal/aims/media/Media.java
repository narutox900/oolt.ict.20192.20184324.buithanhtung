package hust.soict.ictglobal.aims.media;

public abstract class Media implements Comparable<Media> {
    private String title;
    private String category;
    private float cost;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public boolean search(String title) {
        title = title.toLowerCase();
        String name;
        name = this.getTitle().toLowerCase();
        return name.contains(title);
    }

    @Override
    public int compareTo(Media media) {
        try {
            if (media.getClass() == this.getClass())
                if ((media.getCost() == this.getCost() && (media.getTitle().equals(this.getTitle()))))
                    return 0;
                else if (this.getTitle().equals(media.getTitle())) {
                    if (this.getCost() < media.getCost())
                        return -1;
                    else return 1;
                } else return this.getTitle().compareTo(media.getTitle());
            else return -1;
        } catch (NullPointerException e1) {
            System.out.println(e1.getMessage());
        } catch (ClassCastException e2) {
            System.out.println(e2.getMessage());
        }
        return -1;
    }

    @Override
    public boolean equals(Object obj) {
        try {
            if (obj.getClass() == this.getClass()) {
                Media o = (Media) obj;
                if (o.getCost() == this.getCost() && (o.getTitle() == this.getTitle()))
                    return true;
                else return false;
            } else return false;
        } catch (NullPointerException e1) {
            System.out.println(e1.getMessage());
        } catch (ClassCastException e2) {
            System.out.println(e2.getMessage());
        }
        return false;
    }
}

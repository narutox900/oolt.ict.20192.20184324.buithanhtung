package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Disc implements Playable, Comparable<Media> {


    public DigitalVideoDisc(String title) {
        this.setTitle(title);
    }

    public DigitalVideoDisc(String title, float cost) {
        this.setTitle(title);
        this.setCost(cost);
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        this.setTitle(title);
        this.setCategory(category);
        this.setDirector(director);
        this.setLength(length);
        this.setCost(cost);
    }

    @Override
    public void play() throws PlayerException {
        if (this.getLength() <= 0) {
            throw (new PlayerException("ERROR: DVD length is 0"));
        }
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }

    //DVDs are compared by title
    @Override
    public int compareTo(Media o) {
        DigitalVideoDisc object = (DigitalVideoDisc) o;
        if (this.getTitle().equals(object.getTitle()))
            return 0;
        else return this.getTitle().compareTo(object.getTitle());
    }
}

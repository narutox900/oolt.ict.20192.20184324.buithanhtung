package hust.soict.ictglobal.aims.order;

import hust.soict.ictglobal.aims.media.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class Order {

    public static final int MAX_NUMBERS_ORDERED = 10;

    public static final int MAX_LIMITED_ORDERS = 5;

    private static int nbOrders = 0;

    public Order(LocalDate date) {
    }

    public static int getNbOrders() {
        return nbOrders;
    }

    public static void setNbOrders(int nbOrders) {
        Order.nbOrders = nbOrders;
    }

    private ArrayList<Media> itemsOrdered = new ArrayList<Media>();

    private int QtyOrdered = 0;

    private LocalDate dateOrderded;

    public LocalDate getDateOrderded() {
        return dateOrderded;
    }

    public void setDateOrderded(LocalDate dateOrderded) {
        this.dateOrderded = dateOrderded;
    }

    public int getQtyOrdered() {
        return QtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        QtyOrdered = qtyOrdered;
    }

    public static Order createOrder(LocalDate date) {
        if (nbOrders < MAX_LIMITED_ORDERS) {
            Order order = new Order(date);
            order.setDateOrderded(date);
            return order;
        } else {
            System.out.println("Maximum number of orders reached. Please try again another time.");
            return null;
        }
    }

    public void addMedia(Media... list) {
        if (getQtyOrdered() + list.length > 10) {
            System.out.println("That's over the maximum number, please make another order!");
        } else {
            this.itemsOrdered.addAll(Arrays.asList(list));
            setQtyOrdered(getQtyOrdered() + list.length);
        }
    }


    public void removeMedia(int index) {
        if (index > getQtyOrdered())
            System.out.println("There is no item " + index);
        else {
            System.out.println("Item " + index + ". " + itemsOrdered.get(index - 1).getTitle() + " has been deleted.");
            this.itemsOrdered.remove(this.itemsOrdered.get(index - 1));
            setQtyOrdered(getQtyOrdered() - 1);

        }


    }


    public void removeMedia(Media media) {

        if (this.itemsOrdered.remove(media)) {
            setQtyOrdered(getQtyOrdered() - 1);
        } else System.out.println("Can't find item in the order, please try again");


    }

    public boolean equals(DigitalVideoDisc a, DigitalVideoDisc b) {

        return a.getTitle() == b.getTitle();
    }

    public float totalCost() {
        float total = 0;

        for (int i = 0; i < getQtyOrdered(); i++)
            total += itemsOrdered.get(i).getCost();
        return total;
    }

    public void printOrder() {
        System.out.println("*********************Order*********************");
        System.out.println("Date: " + this.getDateOrderded());
        System.out.println("Ordered items: ");
        for (int i = 0; i < this.getQtyOrdered(); i++) {
            if (itemsOrdered.get(i) instanceof DigitalVideoDisc) {
                System.out.println(i + 1 + ". DVD - "
                        + itemsOrdered.get(i).getTitle() + " "
                        + itemsOrdered.get(i).getCategory() + " "
                        + ((DigitalVideoDisc) itemsOrdered.get(i)).getDirector() + " "
                        + ((DigitalVideoDisc) itemsOrdered.get(i)).getLength() + " "
                        + itemsOrdered.get(i).getCost() + "$");
            }
            if (itemsOrdered.get(i) instanceof Book) {
                System.out.println(i + 1 + ". Book - "
                        + itemsOrdered.get(i).getTitle() + " "
                        + itemsOrdered.get(i).getCategory() + " "
                        + ((Book) itemsOrdered.get(i)).getAuthors() + " "
                        + itemsOrdered.get(i).getCost() + "$");
            }
            if (itemsOrdered.get(i) instanceof CompactDisc) {
                System.out.println(i + 1 + ". CD - "
                        + itemsOrdered.get(i).getTitle() + " "
                        + itemsOrdered.get(i).getCategory() + " "
                        + ((CompactDisc) itemsOrdered.get(i)).getArtist() + " "
                        + ((CompactDisc) itemsOrdered.get(i)).getDirector() + " "
                        + ((CompactDisc) itemsOrdered.get(i)).getLength() + " "
                        + itemsOrdered.get(i).getCost() + "$");
            }


        }
        System.out.println("Total cost: " + totalCost());
        System.out.println("**************************************************");
    }

    public Media getALuckyItem() {
        int index = (int) (Math.random() * getQtyOrdered() + 1);
        return itemsOrdered.get(index);
    }

    public void search(String title) {
        int check = 0;
        for (int i = 0; i < getQtyOrdered(); i++) {
            if (itemsOrdered.get(i).search(title)) {
                System.out.println(i + ". " + itemsOrdered.get(i).getTitle());
                check = 1;
            }
        }
        if (check == 0)
            System.out.println("There are no disk with title contain " + title);

    }

    public void play() throws PlayerException {
        if (itemsOrdered.get(itemsOrdered.size() - 1) instanceof DigitalVideoDisc)
            ((DigitalVideoDisc) itemsOrdered.get(itemsOrdered.size() - 1)).play();
        if (itemsOrdered.get(itemsOrdered.size() - 1) instanceof CompactDisc)
            ((CompactDisc) itemsOrdered.get(itemsOrdered.size() - 1)).play();
    }
}
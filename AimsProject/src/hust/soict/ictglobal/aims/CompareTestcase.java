package hust.soict.ictglobal.aims;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.PlayerException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CompareTestcase {
    public static void main(String[] args) throws PlayerException {
        //Test the compareTo method
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("Lion King", "Animation", "Me", 120, 100f);
        dvd1.play();
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("The Mermaid", "Animation", "Not me", 90, 76f);
        dvd2.play();
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("The Lady and the Tramp", "Animation", "Also me", 110, 90f);
        dvd3.play();
        java.util.List<DigitalVideoDisc> dvds = new ArrayList();

        //add the DVD to the ArrayList
        dvds.add(dvd3);
        dvds.add(dvd1);
        dvds.add(dvd2);


        //Iterate through the ArrayList unsorted order
        java.util.Iterator iterator = dvds.iterator();

        System.out.println("-----------------------------------");
        System.out.println("The DVDs currently in the order are: ");

        while (iterator.hasNext()) {
            System.out.println(((DigitalVideoDisc) iterator.next()).getTitle());
        }

        //Sort the DVDs list based on compareTo() method by name
        Collections.sort(dvds);

        //Iterate through the ArrayList in sorted order
        iterator = dvds.iterator();
        System.out.println("After sorting");
        System.out.println("-----------------------------------");
        System.out.println("The DVDs currently in the order are: ");

        while (iterator.hasNext()) {
            System.out.println(((DigitalVideoDisc) iterator.next()).getTitle());
        }


        //add the books to the ArrayList
        java.util.List<Book> books = new ArrayList();
        books.add(new Book("A Clockwork Orange", "Fictional", Arrays.asList("Jimmy"), 50f));
        books.add(new Book("Stellar", "Fictional", Arrays.asList("Tony"), 70f));
        books.add(new Book("Blue", "Fictional", Arrays.asList("Bob"), 55f));


        //Iterate through the ArrayList unsorted order
        iterator = books.iterator();

        System.out.println("-----------------------------------");
        System.out.println("The books currently in the order are: ");

        while (iterator.hasNext()) {
            Book b = (Book) iterator.next();
            System.out.println((b.getTitle() + " " + b.getCost()));
        }

        //Sort the books list based on compareTo() method by cost
        Collections.sort(books);

        //Iterate through the ArrayList in sorted order by cost
        iterator = books.iterator();
        System.out.println("After sorting");
        System.out.println("-----------------------------------");
        System.out.println("The books currently in the order are: ");

        while (iterator.hasNext()) {
            Book b = (Book) iterator.next();
            System.out.println((b.getTitle() + " " + b.getCost()));
        }

        java.util.List<CompactDisc> cds = new ArrayList<>();
        cds.add(new CompactDisc("CD 1", "Category", "Art", "Dir", 50f));
        cds.add(new CompactDisc("CD 2", "Category", "Art", "Dir", 50f));
        cds.add(new CompactDisc("CD 3", "Category", "Art", "Dir", 50f));


        //Iterate through the ArrayList unsorted order
        iterator = cds.iterator();

        System.out.println("-----------------------------------");
        System.out.println("The CDs currently in the order are: ");

        while (iterator.hasNext()) {
            CompactDisc cd = (CompactDisc) iterator.next();
            System.out.println((cd.getTitle() + " " + cd.getNumberoftracks()) + " " + cd.getLength());
        }

        //Sort the CDs list based on compareTo() method by number of tracks and length
        Collections.sort(cds);

        //Iterate through the ArrayList in sorted order by cost
        iterator = cds.iterator();

        System.out.println("After sorting");
        System.out.println("-----------------------------------");
        System.out.println("The CDs currently in the order are: ");

        while (iterator.hasNext()) {
            CompactDisc c = (CompactDisc) iterator.next();
            System.out.println((c.getTitle() + " " + c.getNumberoftracks() + " " + c.getLength()));
        }
    }

}

package hust.soict.ictglobal.date;

import java.util.Date;

public class DateTest {
    public static void main(String[] args){
        MyDate dateA = new MyDate();
        MyDate dateB = new MyDate(28,02,1992);
        MyDate dateC = new MyDate("June 21st 2000");

        dateA.printDate();
        dateB.printDate();
        dateC.printDate();

        dateB.printDate("dd/MM/YYYY");
        dateB.printDate("YYYY/MM/dd");

        dateA.setMonth("July");
        dateA.printDate("dd/MM/YYYY");
        dateA.accept();
        dateA.printDate();
        DateUtils.compareDate(dateB,dateC);
        DateUtils.sortDate(dateA,dateB,dateC);
    }
}

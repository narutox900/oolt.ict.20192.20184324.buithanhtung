package hust.soict.ictglobal.date;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MyDate {

    private int day;
    private int month;
    private int year;

    public MyDate() {

        LocalDate date = LocalDate.now();
        this.day = date.getDayOfMonth();
        this.month = date.getMonthValue();
        this.year = date.getYear();
    }

    public MyDate(int day,int month,int year) {

        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String string){
        String[] arrOfString = string.split(" ", -1);
        switch(arrOfString[0])
        {
            case "January" : month = 1; break;
            case "February" : month = 2; break;
            case "March" : month = 3; break;
            case "April" : month = 4; break;
            case "May" : month = 5; break;
            case "June" : month = 6; break;
            case "July" : month = 7; break;
            case "August" : month = 8; break;
            case "September" : month = 9; break;
            case "October" : month = 10; break;
            case "November" : month = 11; break;
            case "December" : month = 12; break;
            default : System.out.println("Invalid input!");

        }
        setYear(Integer.parseInt(arrOfString[2]));
        setDay(Integer.parseInt(arrOfString[1].substring(0, arrOfString[1].length() - 2)));

    }

    //getter and setter
    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if(month >=1 && month <=12)
            this.month = month;
        else
            System.out.println("Invalid month");
    }
// I can't find any other ways to set the date by string other than this way. Thus, I didn't do setDay and setYear because there are too many cases. Can you tell us some way?
    public void setMonth(String month){
        month = month.toLowerCase();
        switch (month){
            case "january" : this.month = 1; break;
            case "february" : this.month = 2; break;
            case "march" : this.month = 3; break;
            case "april" : this.month = 4; break;
            case "may" : this.month = 5; break;
            case "june" : this.month = 6; break;
            case "july" : this.month = 7; break;
            case "august" : this.month = 8; break;
            case "september" : this.month = 9; break;
            case "october" : this.month = 10; break;
            case "november" : this.month = 11; break;
            case "december" : this.month = 12; break;
            default : System.out.println("Invalid input!");
        }
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        switch (this.month) {
            case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                if(day >= 1 && day <= 31)
                    this.day = day;
                break;
            case 4: case 6: case 9: case 11:
                if(day >= 1 && day <= 30)
                    this.day = day;
                break;
            case 2:
                if (this.year % 4 != 0) {
                    if (day >= 1 && day <= 28)
                        this.day = day;
                } else if (this.year % 100 != 0){
                    if (day >= 1 && day <= 29)
                        this.day = day;
                } else if (this.year % 400 != 0){
                    if (day >= 1 && day <= 28)
                        this.day = day;
                }

                break;

            default:
                System.out.println("Invalid day!");
                break;
        }
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    public void printDate(String format){
        LocalDate date = LocalDate.of(this.year,this.month,this.day);
        System.out.println(date.format(DateTimeFormatter.ofPattern(format)));
    }
    public void printDate(){
        System.out.println(day + "/" + month + "/" + year);
    }

    public void accept() {
        System.out.println("Enter a day (eg.: February 18th 2020): ");
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        MyDate date = new MyDate(string);

        this.day = date.getDay();
        this.month = date.getMonth();
        this.year = date.getYear();

        scanner.close();
    }
}

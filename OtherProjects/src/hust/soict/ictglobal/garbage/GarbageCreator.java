package hust.soict.ictglobal.garbage;

import java.io.*;

public class GarbageCreator {

    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        String testString= "";
        String filename = "garbage.txt";
        File file = new File(filename);
        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);

        String line;
        while((line = br.readLine()) != null ){
            testString +=line;
        }
        System.out.print(testString);
        br.close();
        System.out.println(System.currentTimeMillis()-start); //run infinitely and takes 4-5gb of RAM
    }
}

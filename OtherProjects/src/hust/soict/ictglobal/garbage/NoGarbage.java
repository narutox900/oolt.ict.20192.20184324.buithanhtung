package hust.soict.ictglobal.garbage;

import java.io.*;

public class NoGarbage {
    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        String testString= "";
        String filename = "garbage.txt";
        File file = new File(filename);
        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);

        String line;
        StringBuffer sb = new StringBuffer();
        while((line = br.readLine()) != null ){
            sb.append(line);
        }
        testString = sb.toString();
        System.out.println(testString);
        br.close();
        System.out.println(System.currentTimeMillis()-start);//prints ~90-130
    }
}

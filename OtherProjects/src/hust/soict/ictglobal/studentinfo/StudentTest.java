package hust.soict.ictglobal.studentinfo;

import java.util.Scanner;

public class StudentTest {
    public static void main(String[] args) throws IllegalBirthdayException, IllegalGPAException {
        Scanner keyboard = new Scanner(System.in);
        Student student = new Student();

        System.out.println("Enter student ID: ");
        student.setStudentID(keyboard.nextLine());
        System.out.println("Enter student name: ");
        student.setStudentName(keyboard.nextLine());
        System.out.println("Enter student birthday (dd/mm/yyyy): ");
        student.setBirthday(keyboard.nextLine());
        System.out.println("Enter student GPA: ");
        student.setGpa(keyboard.nextFloat());

        System.out.println("\nStudent info: \n" + student.getStudentName() + " " + student.getStudentID() + " " + student.getBirthday() + " " + student.getGpa());
        keyboard.close();
    }
}

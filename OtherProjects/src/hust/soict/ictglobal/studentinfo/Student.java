package hust.soict.ictglobal.studentinfo;

public class Student {
    private String studentID;
    private String studentName;
    private String birthday;
    private float gpa;

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) throws IllegalBirthdayException {
        try {
            String[] arr = birthday.split("/");
            if (arr.length != 3)
                throw new IllegalBirthdayException("Wrong Format");
            int day = Integer.parseInt(arr[0]);
            int month = Integer.parseInt(arr[1]);
            int year = Integer.parseInt(arr[2]);
            if (month < 0 || month > 12)
                throw new IllegalBirthdayException("Month must be between 1 and 12");

            if (day < 0 || day > 31)
                throw new IllegalBirthdayException("Day must be between 1 and 12");

            switch (month) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    if (day <= 30)
                        break;
                    else throw new IllegalBirthdayException("This month only has 30 days");
                case 2:
                    if (year % 4 != 0) {
                        if (day <= 28)
                            break;
                        else throw new IllegalBirthdayException("This month only has 28 days");
                    } else if (year % 100 != 0) {
                        if (day <= 29)
                            break;
                        else throw new IllegalBirthdayException("This month only has 29 days");
                    } else if (year % 400 != 0) {
                        if (day <= 28)
                            break;
                        else throw new IllegalBirthdayException("This month only has 28 days");
                    } else if (day <= 29)
                        break;
                    else throw new IllegalBirthdayException("This month only has 29 days");
            }
            this.birthday = birthday;
        } catch (IllegalBirthdayException e1) {
            e1.printStackTrace();
        }
    }

    public float getGpa() {
        return gpa;
    }

    public void setGpa(float gpa) throws IllegalGPAException {
        try {
            if ((gpa < 0) || (gpa > 10)) {
                throw new IllegalGPAException();
            } else this.gpa = gpa;
        } catch (IllegalGPAException e1) {
            e1.printStackTrace();
        }
    }
}

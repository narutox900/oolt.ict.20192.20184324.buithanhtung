package hust.soict.ictglobal.studentinfo;

public class IllegalBirthdayException extends Exception {
    public IllegalBirthdayException(){
        super();
    }
    public IllegalBirthdayException(String msg) {
        super(msg);
    }
}

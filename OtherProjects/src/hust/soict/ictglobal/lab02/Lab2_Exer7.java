package hust.soict.ictglobal.lab02;

import java.util.Scanner;
public class Lab2_Exer7 {
    public static void main(String[] args){
                Scanner keyboard = new Scanner(System.in);
                System.out.print("Enter the size of the matrix (mxn) : ");
                int m = keyboard.nextInt();
                int n = keyboard.nextInt();
                int[][] a = new int[m][n];
                int[][] b = new int[m][n];
                int[][] c = new int[m][n];
                System.out.println("Enter matrix a :");
                for(int i = 0 ; i<m ; i++){
                    for (int j = 0 ; j< n;j++){
                        a[i][j]= keyboard.nextInt();
                    }
                }
                System.out.println("Enter matrix b: ");
                for(int i = 0 ; i<m ; i++){
                    for (int j = 0 ; j< n;j++){
                        b[i][j]= keyboard.nextInt();
            }
        }
        System.out.println("The sum of the two matrices is: ");
        for(int i = 0 ; i<m ; i++){
            for (int j = 0 ; j< n;j++){
                c[i][j]= a[i][j]+b[i][j];
                System.out.print(c[i][j] + " ");
            }
            System.out.print("\n");
        }

    }
}

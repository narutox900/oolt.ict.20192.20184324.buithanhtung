package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class Lab2_Exer4 {
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Enter the height of the triangle: ");
        int n = keyboard.nextInt();
        int i,k;
        for ( i = 0; i<n; i++){
            k = 0;
            for (int j = 0;j< n - i-1 ;j++){
                System.out.print(" ");
            }
            while ( k != 2*i+1 ){
                System.out.print("*");
                k++;
            }
            System.out.print("\n");
        }
    }
}

package hust.soict.ictglobal.lab02;

import java.util.Scanner;
public class Lab2_Exer6 {
    public static void main(String[] args){
        int n,sum=0;
        float average;
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter the number of elements you want in the array: ");
        n = keyboard.nextInt();
        int[] array = new int[n];
        System.out.println("Enter all the elements: ");
        for (int i = 0 ; i<n ;i++){
            array[i] = keyboard.nextInt();
            sum+= array[i];
        }
        System.out.println("The sum is:" + sum);
        average = (float)sum/n ;
        System.out.println("The average is:" + average);

    }
}

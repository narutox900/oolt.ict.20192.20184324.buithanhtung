package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class Lab2_Exer5 {
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);
        int month,year;
        do {
            System.out.print("Please enter the month (1-12): ");
            month = keyboard.nextInt();
            System.out.print("Please enter the year: ");
            year = keyboard.nextInt();
            if (month<1 || month>12 || year < 1)
                System.out.println("The data is invalid, please input again");
        } while (month<1 || month > 12 || year < 1);
        if (month == 1 || month ==  3 ||month == 5 || month == 7 || month == 8 || month ==  10 || month == 12){
            System.out.println("There are 31 days in that month");
        } else if (month == 4 || month == 6 || month == 9 || month == 11){
            System.out.println("There are 31 days in that month");
        } else if ((year%4) != 0 )
        {
            System.out.println("There are 28 days in that month");
        } else if ((year%100) != 0)
        {
            System.out.println("There are 29 days in that month");
        } else if ((year%400) != 0){
            System.out.println("There are 28 days in that month");
        } else System.out.println("There are 31 days in that month");
    }
}

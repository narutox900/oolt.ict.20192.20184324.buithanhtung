package hust.soict.ictglobal.lab01;

import java.util.Scanner;

public class Lab1_Exer5 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter 2 double numbers: ");
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        System.out.println("The sum of " + a+ " and "+b + " is " + (a+b));
        System.out.println("The difference of " + a + " and " + b + " is " + (a-b));
        System.out.println("The product of " + a + " and " + b + " is " + (a*b));
        if (b!=0){
            System.out.println("The quotient of " + a + " and " + b + " is " +(a/b));
        }
        else System.out.println(a + " can't be divided by " + b);

    }
}
